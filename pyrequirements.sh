#!/bin/bash
if [ "$EUID" -ne 0 ]
then
	echo -e "Please run as root"
	exit 3
fi
if [ "$1" == "" ]
then
	python="python3"
	pip="pip3"
elif [ "$1" == "python3" ]
then
	python="python3"
	pip="pip3"
else
	python="python2"
	pip="pip2"
fi
read -ep "insert the script:  " script
if [ -f $script ]
then
	$python $script 2> errore 
	if [ $? -eq 1 ]
	then
		while [ "$(grep -o "ModuleNotFoundError" errore)" == "ModuleNotFoundError" ]
		do
			modulo=$(grep "ModuleNotFoundError" errore| cut -d" " -f5| tr "'" " ")
			$pip install $modulo
			if [ ! $? -eq 0 ]
			then
				printf "module $modulo doesn't exist"
				exit 2
			fi

			$python $script 2> errore 
		done
		rm errore	
	else
		printf "there isn't ModuleNotFoundError"
	fi	
else
	printf "file doesn't exist\n"
	exit 1
fi
